package com.inclusivitysolutions.codingassignmentrathin.config;

import cucumber.api.TypeRegistry;
import cucumber.api.TypeRegistryConfigurer;
import io.cucumber.datatable.DataTableType;

import java.util.Locale;
import java.util.Map;

public class FieldTransformer implements TypeRegistryConfigurer {
    @Override
    public Locale locale() {
        return Locale.ENGLISH;
    }

    @Override
    public void configureTypeRegistry(TypeRegistry typeRegistry) {
        typeRegistry.defineDataTableType(new DataTableType(ItemRow.class,
                (Map<String, String> row) -> {
                    String item = row.get("item");
                    double unitPrice = Double.valueOf(row.get("unitPrice"));
                    int quantity = Integer.valueOf(row.get("quantity"));
                    return new ItemRow(item, unitPrice, quantity);
                }));
    }
}
