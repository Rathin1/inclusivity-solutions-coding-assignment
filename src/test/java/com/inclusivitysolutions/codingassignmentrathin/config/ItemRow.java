package com.inclusivitysolutions.codingassignmentrathin.config;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ItemRow {
    private String item;
    private double unitPrice;
    private int quantity;
}
