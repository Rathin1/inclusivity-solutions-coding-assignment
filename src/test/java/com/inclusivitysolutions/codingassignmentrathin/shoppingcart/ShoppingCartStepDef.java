package com.inclusivitysolutions.codingassignmentrathin.shoppingcart;

import com.inclusivitysolutions.codingassignmentrathin.model.Item;
import com.inclusivitysolutions.codingassignmentrathin.model.ShoppingCart;
import com.inclusivitysolutions.codingassignmentrathin.config.ItemRow;
import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.inclusivitysolutions.codingassignmentrathin.model.Money.money;
import static com.inclusivitysolutions.codingassignmentrathin.model.Money.zero_zar;
import static org.junit.Assert.assertEquals;

public class ShoppingCartStepDef {
    private ShoppingCart actualShoppingCart;

    @Given("an empty shopping cart")
    public void anEmptyShoppingCart() {
        actualShoppingCart = new ShoppingCart();
    }

    @And("a tax rate of {double}")
    public void aTaxRateOf(double taxRate) {
        actualShoppingCart.setTaxRate(taxRate/100);
    }

    @When("the user adds the following items to the shopping cart")
    public void theUserAddsTheFollowingItemsToTheShoppingCart(List<ItemRow> items) {
        for (ItemRow itemRow : items) {
            actualShoppingCart.add(new Item(itemRow.getItem(), money(itemRow.getUnitPrice())), itemRow.getQuantity());
        }
    }

    @Then("the shopping cart should contain {int} Dove Soaps each with a unit price of {double} and total price should equal {double}")
    public void theShoppingCartShouldContainDoveSoapsEachWithAUnitPriceOfAndTotalPriceShouldEqual(int expectedQuantity, double unitPrice, double expectedTotalPrice) {
        Item dove = new Item("Dove Soap", money(unitPrice));

        Map<Item, Integer> doveItems = new HashMap<>();
        doveItems.put(dove, expectedQuantity);
        ShoppingCart expectedShoppingCart = new ShoppingCart(doveItems, money(expectedTotalPrice), zero_zar());

        assertEquals(expectedShoppingCart, actualShoppingCart);
    }

    @Then("the shopping cart should contain {int} Dove Soaps each with a unit price of {double}, {int} Axe Deo’s each with a unit price of {double}, total tax amount should equal {double} and the shopping cart’s total price should equal {double}")
    public void theShoppingCartShouldContainDoveSoapsEachWithAUnitPriceOfAxeDeoSEachWithAUnitPriceOfTotalTaxAmountShouldEqualAndTheShoppingCartSTotalPriceShouldEqual(int quantityOfDoveSoap, double doveSoapUnitPrice, int quantityOfAxeDeo, double axeDeoUnitPrice, double totalTax, double totalPrice) {
        Map<Item, Integer> numberOfItems = new HashMap<>();
        Item expectedDove = new Item("Dove Soap", money(doveSoapUnitPrice));
        Item expectedAxe = new Item("Axe Deo", money(axeDeoUnitPrice));
        numberOfItems.put(expectedDove, quantityOfDoveSoap);
        numberOfItems.put(expectedAxe, quantityOfAxeDeo);
        ShoppingCart expectedShoppingCart = new ShoppingCart(numberOfItems, money(totalPrice), money(totalTax), 0.125);
        assertEquals(expectedShoppingCart, actualShoppingCart);
    }

}
