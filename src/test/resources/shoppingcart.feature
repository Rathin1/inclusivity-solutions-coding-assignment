Feature: Shopping Cart
  Shopping Cart testing

  Scenario:  Add products to the shopping cart
    Given an empty shopping cart
    When the user adds the following items to the shopping cart
      | item      | unitPrice | quantity |
      | Dove Soap | 39.99     | 5        |
    Then the shopping cart should contain 5 Dove Soaps each with a unit price of 39.99 and total price should equal 199.95

  Scenario: Add additional products of the same type to the shopping cart.
    Given an empty shopping cart
    When the user adds the following items to the shopping cart
      | item      | unitPrice | quantity |
      | Dove Soap | 39.99     | 5        |
      | Dove Soap | 39.99     | 3        |
    Then the shopping cart should contain 8 Dove Soaps each with a unit price of 39.99 and total price should equal 319.92

  Scenario: Calculate the tax rate of the shopping cart with multiple items
    Given an empty shopping cart
    And a tax rate of 12.5
    When the user adds the following items to the shopping cart
      | item      | unitPrice | quantity |
      | Dove Soap | 39.99     | 2        |
      | Axe Deo   | 99.99     | 2        |
    Then the shopping cart should contain 2 Dove Soaps each with a unit price of 39.99, 2 Axe Deo’s each with a unit price of 99.99, total tax amount should equal 35.00 and the shopping cart’s total price should equal 314.96


